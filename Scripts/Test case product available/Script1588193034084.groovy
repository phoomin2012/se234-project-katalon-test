import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String formatNumber(string) {
	double amount = Double.parseDouble(number)
	DecimalFormat formatter = new DecimalFormat("#,###.00")
	return formatter.format(amount)
}

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.206.119.126:8101/')

WebUI.setText(findTestObject('Page_ProjectBackend/input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.verifyElementAttributeValue(findTestObject('Page_ProjectBackend/Product Box/Product image'), 'src', imageLocation, 
    0)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Product Box/Product name'), name)

WebUI.verifyElementText(findTestObject('Product Box/Product description'), description)

price = "Price is " + formatNumber(price) + " THB"

WebUI.verifyElementText(findTestObject('Product Box/Product price'), price)

WebUI.closeBrowser()

