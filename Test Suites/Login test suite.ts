<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>10446fe4-b08a-415a-8e00-85974401dd40</testSuiteGuid>
   <testCaseLink>
      <guid>3658160b-93ab-40eb-9e78-2458219a5fc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/1. Empty username-password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>527801fb-048c-4889-9001-dba74e30dd19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/2. Empty username</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de65a023-f3db-4fa7-b09c-bc21c8768c00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/3. Empty password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55b4a02f-7a9c-4b88-999e-2f4e9a20b1d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/4. Incorrect username-password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>445ee45d-bb4c-4106-b3a7-de79900c86bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/5. Correct username-password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55367290-ecb7-41ed-b795-fd8742663b78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/6. Test login success</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5afb8178-8b33-4dd2-9730-5d7be8c5e3c3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/User</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>5afb8178-8b33-4dd2-9730-5d7be8c5e3c3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>3f035efa-6344-44c7-aa1f-2ae02067ca14</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>5afb8178-8b33-4dd2-9730-5d7be8c5e3c3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>aba02a68-97bc-4590-a7de-5b990e253c44</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
